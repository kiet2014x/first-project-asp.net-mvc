using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Model.EF
{
    public partial class ModelDBContext : DbContext
    {
        public ModelDBContext()
            : base("ModelDBContext")
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ModelDBContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
