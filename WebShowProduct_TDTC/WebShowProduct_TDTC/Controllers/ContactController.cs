﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;
using System.Web.Script.Serialization;
using System.Configuration;
using System.IO;


namespace WebShowProduct_TDTC.Controllers
{
    public class ContactController : Controller
    {


        // GET: Contact
        public ActionResult Index()
        {
            var model = new ContactDao().GetActiveContact();
            return View(model);
        }

        [HttpPost]
        public ActionResult Send(string name, string phone, string mail, string content)
        {
            var feedback = new Feedback();
            feedback.Name = name;
            feedback.Phone = phone;
            feedback.Email = mail;
            feedback.Description = content;

            //var id = new ContactDao().InsertFeedback(feedback);
            //if (id > 0)
            //    return Json(
            //        new
            //        {
            //            status = true

            //        }

            //        );
            //else
            //    return Json(
            //        new{
            //            status = false
            //            }
            //        );


            string contents = System.IO.File.ReadAllText(Server.MapPath("~/Content/template/neworderMail.html"));

            contents = contents.Replace("{{CustomerName}}", name);
            contents = contents.Replace("{{Phone}}", phone);
            contents = contents.Replace("{{Email}}", mail);
            contents = contents.Replace("{{Detail}}", content);
            var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();
            new MailHeelper().SendMail(toEmail, "Phản hồi mới", contents);
            return Redirect("/hoan-thanh");
        }


       
       
            
            
    }
}