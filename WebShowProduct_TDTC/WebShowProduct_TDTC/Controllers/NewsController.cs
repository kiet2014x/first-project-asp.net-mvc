﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Controllers
{
    public class NewsController : Controller
    {

        ShowProduct_TDTCEntities _db = new ShowProduct_TDTCEntities();
        // GET: News
        public ActionResult Index(string meta)
        {
          
            var v = from t in _db.news
                    where t.Meta == meta
                    select t;
            return View(v.FirstOrDefault());

        }


        public ActionResult Detail(long id)
        {
            var v = from t in _db.news
                    where t.Id == id
                    select t;
            return View(v.FirstOrDefault());
        }
    }
}