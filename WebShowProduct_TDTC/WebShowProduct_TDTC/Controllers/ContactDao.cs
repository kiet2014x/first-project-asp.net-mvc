﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebShowProduct_TDTC.Models;



namespace WebShowProduct_TDTC.Controllers
{
    public class ContactDao
    {
        ShowProduct_TDTCEntities db = null;
        public ContactDao()
        {
            db = new ShowProduct_TDTCEntities();
        }


        public Contact GetActiveContact()
        {
            return db.Contacts.Single(x => x.Status == true);



        }

        public int InsertFeedback(Feedback fb)
        {
            db.Feedbacks.Add(fb);
            db.SaveChanges();
            return fb.Id;
        }
    }
}
