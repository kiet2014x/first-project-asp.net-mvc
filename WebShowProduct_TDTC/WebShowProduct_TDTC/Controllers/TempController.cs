﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Controllers
{
    public class TempController : Controller
    {
        ShowProduct_TDTCEntities _db = new ShowProduct_TDTCEntities();
        // GET: Temp
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult getHome()
        {
            return PartialView();
        }
        public ActionResult getAbout()
        {
            return PartialView();
        }
        public ActionResult Contact()
        {
            return PartialView();
        }


        public ActionResult News()
        {
            return PartialView();
        }



        public ActionResult getMenu()
        {
            
            var v = from t in _db.menus
                    where t.Hide == true
                    orderby t.Order ascending
                    select t;
            return PartialView(v.ToList());
        }
    }
}