﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;


namespace WebShowProduct_TDTC.Controllers
{
    public class DefaultController : Controller
    {
        ShowProduct_TDTCEntities _db = new ShowProduct_TDTCEntities();
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            return PartialView();
        }
        public ActionResult Contact()
        {
            return PartialView();
        }
        public ActionResult getCategory()
        {
            
            ViewBag.meta = "san-pham";
            var v = from t in _db.categories
                    where t.Hide == true
                    orderby t.Order ascending
                    select t;
            return PartialView(v.ToList());
        }
        public ActionResult getProduct(int id)
        {
            ViewBag.meta = "san-pham";
            var v = from t in _db.products
                    where t.CategoryId == id && t.Hide == true
                    orderby t.Order ascending
                    select t;
            return PartialView(v.ToList());
        }

       
        public ActionResult getProductCarousel()
        {
            var v = from t in _db.products
                    where t.Hide == true
                    select t;
            return PartialView(v.ToList());
        }
        public ActionResult getAboutCategory()
        {
            var v = from t in _db.categories
                    where t.Hide == true
                    orderby t.Order ascending
                    select t;
            return PartialView(v.ToList());
        }
        public ActionResult getQuestion()
        {
            var v = from t in _db.HomeQuestions
                    select t;
            return PartialView(v.ToList());
        }

        public ActionResult GetNewsTop()
        {

            var v = from t in _db.NewsIntroductionTops
                    select t;
            return PartialView(v.ToList());
        }

        public ActionResult getNewsDataNumbermiddle()
        {
            var v = from t in _db.NewsDataNumberMiddles
                    select t;
            return PartialView(v.ToList());
        }

        public ActionResult getNewsYearsmiddle2()
        {
            var v = from t in _db.NewsYearsNumberMiddle2
                    select t;
            return PartialView(v.ToList());
        }
        
            public ActionResult getNewsPersonel()
        {
            var v = from t in _db.NewsPersonels
                    select t;
            return PartialView(v.ToList());
        }
        public ActionResult getAnswer(int Id)
        {
            var v = from t in _db.HomeAnswers
                    where t.QuestionID==Id
                    select t;
            return PartialView(v.ToList());
        }


        public ActionResult GetNewsListTop()
        {
            ViewBag.meta = "tin-tuc";
            var v = from t in _db.news
                    where t.Hide == true
                    orderby t.Order ascending
                    select t;
            return PartialView(v.ToList());
        }
        public ActionResult getDetailNews(int id)
        {
            ViewBag.meta = "tin-tuc";
            var v = from t in _db.news
                    
                    where t.Id == id && t.Hide == true
                    orderby t.Order ascending
                    select t;
            return PartialView(v.ToList());
        }
        [HttpGet]
        public ActionResult GetNewsListMiddle()
        {
            var v = from t in _db.news
                    where t.Hide == true
                    orderby t.Order ascending
                    select t;
            return PartialView(v.ToList());
        }


        public ActionResult getAboutIndex()
        {
            var v = from t in _db.Contactindexes
                    select t;
            return PartialView(v.ToList());
        }



    }
}