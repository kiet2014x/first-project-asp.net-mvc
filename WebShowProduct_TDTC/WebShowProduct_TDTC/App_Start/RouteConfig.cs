﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebShowProduct_TDTC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute("Temp", "{type}/{meta}",
            //     new { controller = "temp", action = "getHome", meta = UrlParameter.Optional },
            //     new RouteValueDictionary
            //     {
            //    { "type", "lien-he" }
            //     },
            //     namespaces: new[] { "WebShowProduct_TDTC.Controllers" });

            //routes.MapRoute("Temp", "{type}/{meta}",
            //     new { controller = "temp", action = "getHome", meta = UrlParameter.Optional },
            //     new RouteValueDictionary
            //     {
            //    { "type", "gioi-thieu" }
            //     },
            //     namespaces: new[] { "WebShowProduct_TDTC.Controllers" });

         

            routes.MapRoute("Temp", "{type}/{meta}",
                 new { controller = "temp", action = "getHome", meta = UrlParameter.Optional },
                 new RouteValueDictionary
                 {
                { "type", "trang-chu" }
                 },
                 namespaces: new[] { "WebShowProduct_TDTC.Controllers" });


            routes.MapRoute("Contact", "{type}/{meta}",
             new { controller = "Contact", action = "Index", meta = UrlParameter.Optional },
             new RouteValueDictionary
             {
                { "type", "ho-tro" }
             },
             namespaces: new[] { "WebShowProduct_TDTC.Controllers" });

            routes.MapRoute("Send", "{type}/{meta}",
             new { Controller = "Contact", action = "Send", meta= UrlParameter.Optional},
            new RouteValueDictionary
             {
                { "type", "ho-tro" }
             },
            namespaces: new[] { "WebShowProduct_TDTC.Controllers" }) ;

            routes.MapRoute("Sends", "{type}/{meta}",
             defaults: new { controller = "Contact", action = "Send", meta = UrlParameter.Optional },
             new RouteValueDictionary
             {
                { "type", "ho-tro" }
             },
             namespaces: new[] { "WebShowProduct_TDTC.Controllers" });


            routes.MapRoute("About", "{type}/{meta}",
            new { controller = "temp", action = "getAbout", meta = UrlParameter.Optional },
            new RouteValueDictionary
            {
                { "type", "gioi-thieu" }
            },
            namespaces: new[] { "WebShowProduct_TDTC.Controllers" });

            routes.MapRoute("News", "{type}/{meta}",
            new { controller = "temp", action = "News", meta = UrlParameter.Optional },
            new RouteValueDictionary
            {
                { "type", "tin-tuc" }
            },
            namespaces: new[] { "WebShowProduct_TDTC.Controllers" });

            routes.MapRoute("DetailNews", "{type}/{meta}/{id}",
           new { controller = "News", action = "Detail", id = UrlParameter.Optional },
           new RouteValueDictionary
           {
                { "type", "tin-tuc" }
           },
           namespaces: new[] { "WebShowProduct_TDTC.Controllers" });



            routes.MapRoute("Product", "{type}/{meta}",
                  new { controller = "product", action = "Index", meta = UrlParameter.Optional },
                  new RouteValueDictionary
                  {
                { "type", "san-pham" }
                  },
                  namespaces: new[] { "WebShowProduct_TDTC.Controllers" });

            routes.MapRoute("Detail", "{type}/{meta}/{id}",
              new { controller = "product", action = "Detail", id = UrlParameter.Optional },
              new RouteValueDictionary
              {
                { "type", "san-pham" }
              },
              namespaces: new[] { "WebShowProduct_TDTC.Controllers" });


          

            routes.MapRoute(
                name: "MyHome",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "WebShowProduct_TDTC.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                    namespaces: new[] { "WebShowProduct_TDTC.Controllers" }
            );




        }
    }
}
