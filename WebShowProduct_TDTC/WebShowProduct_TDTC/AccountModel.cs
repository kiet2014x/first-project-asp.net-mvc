﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC
{
    public class AccountModel
    {
        private ShowProduct_TDTCEntities context = new ShowProduct_TDTCEntities();
        public bool Login(string userName, string Password)
        {
            object[] sqlParams =
             {
                new SqlParameter("@userName", userName),
                new SqlParameter("@Password", Password),
             };
            var res =
                 context.Database.SqlQuery<bool>("Sp_Account_Login @userName,@Password", sqlParams).SingleOrDefault();
            return res;
        }
    }
}