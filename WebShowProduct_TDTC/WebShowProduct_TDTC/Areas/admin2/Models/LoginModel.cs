﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebShowProduct_TDTC.Areas.admin2.Models
{
    public class LoginModel
    {
        [Required]
        public string userName { set; get; }

     
        public string Password { set; get; }

        public bool RememberMe { set; get; }
    }
}