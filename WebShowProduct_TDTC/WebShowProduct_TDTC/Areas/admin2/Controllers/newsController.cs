﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Help;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class newsController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/news
        public ActionResult Index()
        {
            return View(db.news.ToList());
        }

        // GET: admin2/news/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            news news = db.news.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }



        // GET: admin2/news/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/news/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]


        public ActionResult Create([Bind(Include = "Id,Name,Image,Description,Detail,Meta,Hide,Order,DateBegin")] news news, HttpPostedFileBase img)
        {
            var path = "";
            var filename = "";
            if (ModelState.IsValid)
            {

                if (img != null)
                {
                    //filename = Guid.NewGuid().ToString() + img.FileName
                    filename = img.FileName;
                    path = Path.Combine(Server.MapPath("~/Content/images/"), filename);

                    img.SaveAs(path);
                    news.Image = filename;
                }
                else
                {
                    news.Image = "LOgo.jqg";
                }
                news.DateBegin = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                news.Meta = Functions.ConvertToUnSign(news.Name);
                db.news.Add(news);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(news);
        }

        // GET: admin2/news/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            news news = db.news.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: admin2/news/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,Name,Image,Description,Detail,Meta,Hide,Order,DateBegin")] news news, HttpPostedFileBase img)
        {
            var path = "";
            var filename = "";
            news temp = getById(news.Id);
            if (ModelState.IsValid)
            {

                if (img != null)
                {
                    //filename = guid.NewGuid().ToString + img.FileName;
                    filename = DateTime.Now.ToString("dd-MM-yy-hh-mm-ss-") + img.FileName;
                    path = Path.Combine(Server.MapPath("~/Content/images/"), filename);
                    img.SaveAs(path);
                    temp.Image = filename;

                }
                temp.Name = news.Name;
                temp.Description = news.Description;
                temp.Detail = news.Detail;
                temp.Meta = Functions.ConvertToUnSign(news.Meta);
                temp.Hide = news.Hide;
                temp.Order = news.Order;
                db.Entry(temp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(news);
        }

        public news getById(long id)
        {
            return db.news.Where(x => x.Id == id).FirstOrDefault();
        }

        // GET: admin2/news/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            news news = db.news.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: admin2/news/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            news news = db.news.Find(id);
            db.news.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
