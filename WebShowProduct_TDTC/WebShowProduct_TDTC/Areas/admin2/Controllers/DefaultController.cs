﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    [Authorize]
    public class DefaultController : Controller
    {
        // GET: admin2/Default
        public ActionResult Index()
        {
            return View();
        }
    }
}