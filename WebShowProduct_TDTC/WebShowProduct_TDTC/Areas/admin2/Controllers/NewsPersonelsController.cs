﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class NewsPersonelsController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/NewsPersonels
        public ActionResult Index()
        {
            return View(db.NewsPersonels.ToList());
        }

        // GET: admin2/NewsPersonels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsPersonel newsPersonel = db.NewsPersonels.Find(id);
            if (newsPersonel == null)
            {
                return HttpNotFound();
            }
            return View(newsPersonel);
        }

        // GET: admin2/NewsPersonels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/NewsPersonels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Image")] NewsPersonel newsPersonel)
        {
            if (ModelState.IsValid)
            {
                db.NewsPersonels.Add(newsPersonel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(newsPersonel);
        }

        // GET: admin2/NewsPersonels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsPersonel newsPersonel = db.NewsPersonels.Find(id);
            if (newsPersonel == null)
            {
                return HttpNotFound();
            }
            return View(newsPersonel);
        }

        // POST: admin2/NewsPersonels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Image")] NewsPersonel newsPersonel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(newsPersonel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(newsPersonel);
        }

        // GET: admin2/NewsPersonels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsPersonel newsPersonel = db.NewsPersonels.Find(id);
            if (newsPersonel == null)
            {
                return HttpNotFound();
            }
            return View(newsPersonel);
        }

        // POST: admin2/NewsPersonels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsPersonel newsPersonel = db.NewsPersonels.Find(id);
            db.NewsPersonels.Remove(newsPersonel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
