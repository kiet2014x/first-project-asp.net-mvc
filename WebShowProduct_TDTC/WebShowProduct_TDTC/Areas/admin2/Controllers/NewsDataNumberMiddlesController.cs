﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class NewsDataNumberMiddlesController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/NewsDataNumberMiddles
        public ActionResult Index()
        {
            return View(db.NewsDataNumberMiddles.ToList());
        }

        // GET: admin2/NewsDataNumberMiddles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsDataNumberMiddle newsDataNumberMiddle = db.NewsDataNumberMiddles.Find(id);
            if (newsDataNumberMiddle == null)
            {
                return HttpNotFound();
            }
            return View(newsDataNumberMiddle);
        }

        // GET: admin2/NewsDataNumberMiddles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/NewsDataNumberMiddles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,DataName,DataNumber,DataImage,Order")] NewsDataNumberMiddle newsDataNumberMiddle)
        {
            if (ModelState.IsValid)
            {
                db.NewsDataNumberMiddles.Add(newsDataNumberMiddle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(newsDataNumberMiddle);
        }

        // GET: admin2/NewsDataNumberMiddles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsDataNumberMiddle newsDataNumberMiddle = db.NewsDataNumberMiddles.Find(id);
            if (newsDataNumberMiddle == null)
            {
                return HttpNotFound();
            }
            return View(newsDataNumberMiddle);
        }

        // POST: admin2/NewsDataNumberMiddles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,DataName,DataNumber,DataImage,Order")] NewsDataNumberMiddle newsDataNumberMiddle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(newsDataNumberMiddle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(newsDataNumberMiddle);
        }

        // GET: admin2/NewsDataNumberMiddles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsDataNumberMiddle newsDataNumberMiddle = db.NewsDataNumberMiddles.Find(id);
            if (newsDataNumberMiddle == null)
            {
                return HttpNotFound();
            }
            return View(newsDataNumberMiddle);
        }

        // POST: admin2/NewsDataNumberMiddles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsDataNumberMiddle newsDataNumberMiddle = db.NewsDataNumberMiddles.Find(id);
            db.NewsDataNumberMiddles.Remove(newsDataNumberMiddle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
