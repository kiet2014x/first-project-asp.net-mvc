﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class NewsIntroductionTopsController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/NewsIntroductionTops
        public ActionResult Index()
        {
            return View(db.NewsIntroductionTops.ToList());
        }

        // GET: admin2/NewsIntroductionTops/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsIntroductionTop newsIntroductionTop = db.NewsIntroductionTops.Find(id);
            if (newsIntroductionTop == null)
            {
                return HttpNotFound();
            }
            return View(newsIntroductionTop);
        }

        // GET: admin2/NewsIntroductionTops/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/NewsIntroductionTops/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Detail,Image")] NewsIntroductionTop newsIntroductionTop)
        {
            if (ModelState.IsValid)
            {
                db.NewsIntroductionTops.Add(newsIntroductionTop);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(newsIntroductionTop);
        }

        // GET: admin2/NewsIntroductionTops/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsIntroductionTop newsIntroductionTop = db.NewsIntroductionTops.Find(id);
            if (newsIntroductionTop == null)
            {
                return HttpNotFound();
            }
            return View(newsIntroductionTop);
        }

        // POST: admin2/NewsIntroductionTops/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Detail,Image")] NewsIntroductionTop newsIntroductionTop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(newsIntroductionTop).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(newsIntroductionTop);
        }

        // GET: admin2/NewsIntroductionTops/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsIntroductionTop newsIntroductionTop = db.NewsIntroductionTops.Find(id);
            if (newsIntroductionTop == null)
            {
                return HttpNotFound();
            }
            return View(newsIntroductionTop);
        }

        // POST: admin2/NewsIntroductionTops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsIntroductionTop newsIntroductionTop = db.NewsIntroductionTops.Find(id);
            db.NewsIntroductionTops.Remove(newsIntroductionTop);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
