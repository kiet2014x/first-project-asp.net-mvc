﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class ContactindexesController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/Contactindexes
        public ActionResult Index()
        {
            return View(db.Contactindexes.ToList());
        }

        // GET: admin2/Contactindexes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contactindex contactindex = db.Contactindexes.Find(id);
            if (contactindex == null)
            {
                return HttpNotFound();
            }
            return View(contactindex);
        }

        // GET: admin2/Contactindexes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/Contactindexes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NameChinhanh,DiaChi,SDT")] Contactindex contactindex)
        {
            if (ModelState.IsValid)
            {
                db.Contactindexes.Add(contactindex);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contactindex);
        }

        // GET: admin2/Contactindexes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contactindex contactindex = db.Contactindexes.Find(id);
            if (contactindex == null)
            {
                return HttpNotFound();
            }
            return View(contactindex);
        }

        // POST: admin2/Contactindexes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NameChinhanh,DiaChi,SDT")] Contactindex contactindex)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contactindex).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contactindex);
        }

        // GET: admin2/Contactindexes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contactindex contactindex = db.Contactindexes.Find(id);
            if (contactindex == null)
            {
                return HttpNotFound();
            }
            return View(contactindex);
        }

        // POST: admin2/Contactindexes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contactindex contactindex = db.Contactindexes.Find(id);
            db.Contactindexes.Remove(contactindex);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
