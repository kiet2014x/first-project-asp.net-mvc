﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class HomeQuestionController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/HomeQuestion
        public ActionResult Index()
        {
            return View(db.HomeQuestions.ToList());
        }

        // GET: admin2/HomeQuestion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeQuestion homeQuestion = db.HomeQuestions.Find(id);
            if (homeQuestion == null)
            {
                return HttpNotFound();
            }
            return View(homeQuestion);
        }

        // GET: admin2/HomeQuestion/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/HomeQuestion/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Question")] HomeQuestion homeQuestion)
        {
            if (ModelState.IsValid)
            {
                db.HomeQuestions.Add(homeQuestion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(homeQuestion);
        }

        // GET: admin2/HomeQuestion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeQuestion homeQuestion = db.HomeQuestions.Find(id);
            if (homeQuestion == null)
            {
                return HttpNotFound();
            }
            return View(homeQuestion);
        }

        // POST: admin2/HomeQuestion/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Question")] HomeQuestion homeQuestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(homeQuestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(homeQuestion);
        }

        // GET: admin2/HomeQuestion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeQuestion homeQuestion = db.HomeQuestions.Find(id);
            if (homeQuestion == null)
            {
                return HttpNotFound();
            }
            return View(homeQuestion);
        }

        // POST: admin2/HomeQuestion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HomeQuestion homeQuestion = db.HomeQuestions.Find(id);
            db.HomeQuestions.Remove(homeQuestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
