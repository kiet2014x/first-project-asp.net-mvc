﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class NewsYearsNumberMiddle2Controller : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/NewsYearsNumberMiddle2
        public ActionResult Index()
        {
            return View(db.NewsYearsNumberMiddle2.ToList());
        }

        // GET: admin2/NewsYearsNumberMiddle2/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsYearsNumberMiddle2 newsYearsNumberMiddle2 = db.NewsYearsNumberMiddle2.Find(id);
            if (newsYearsNumberMiddle2 == null)
            {
                return HttpNotFound();
            }
            return View(newsYearsNumberMiddle2);
        }

        // GET: admin2/NewsYearsNumberMiddle2/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/NewsYearsNumberMiddle2/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,YearsNumber,YearsDescription")] NewsYearsNumberMiddle2 newsYearsNumberMiddle2)
        {
            if (ModelState.IsValid)
            {
                db.NewsYearsNumberMiddle2.Add(newsYearsNumberMiddle2);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(newsYearsNumberMiddle2);
        }

        // GET: admin2/NewsYearsNumberMiddle2/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsYearsNumberMiddle2 newsYearsNumberMiddle2 = db.NewsYearsNumberMiddle2.Find(id);
            if (newsYearsNumberMiddle2 == null)
            {
                return HttpNotFound();
            }
            return View(newsYearsNumberMiddle2);
        }

        // POST: admin2/NewsYearsNumberMiddle2/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,YearsNumber,YearsDescription")] NewsYearsNumberMiddle2 newsYearsNumberMiddle2)
        {
            if (ModelState.IsValid)
            {
                db.Entry(newsYearsNumberMiddle2).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(newsYearsNumberMiddle2);
        }

        // GET: admin2/NewsYearsNumberMiddle2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsYearsNumberMiddle2 newsYearsNumberMiddle2 = db.NewsYearsNumberMiddle2.Find(id);
            if (newsYearsNumberMiddle2 == null)
            {
                return HttpNotFound();
            }
            return View(newsYearsNumberMiddle2);
        }

        // POST: admin2/NewsYearsNumberMiddle2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsYearsNumberMiddle2 newsYearsNumberMiddle2 = db.NewsYearsNumberMiddle2.Find(id);
            db.NewsYearsNumberMiddle2.Remove(newsYearsNumberMiddle2);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
