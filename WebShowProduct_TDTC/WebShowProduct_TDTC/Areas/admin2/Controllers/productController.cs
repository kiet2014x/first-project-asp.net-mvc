﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Help;
using WebShowProduct_TDTC.Models;


namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class productController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/product
        public ActionResult Index(long? id = null)
        {
            getCategory(id);
            //return View(db.products.ToList());
            return View();
        }
        public void getCategory(long? selectedId = null)
        {
            ViewBag.Category = new SelectList(db.categories.Where(x => x.Hide == true)
                .OrderBy(x => x.Order), "Id", "Name", selectedId);
        }
        public ActionResult getProduct(long? id)
        {
            if (id == null)
            {
                var v = db.products.OrderBy(x => x.Order).ToList();
                return PartialView(v);
            }
            var m = db.products.Where(x => x.CategoryId == id).OrderBy(x => x.Order).ToList();
            return PartialView(m);
        }
        // GET: admin2/product/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: admin2/product/Create
        public ActionResult Create()
        {
            getCategory();
            return View();
        }

        // POST: admin2/product/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Id,Name,Subname,Price,Images,Detail,Description,Meta,Hide,Order,DateBegin,CategoryId")] product product, HttpPostedFileBase img)
        {
            try
            {
                var path = "";
                var filename = "";
                if (ModelState.IsValid)
                {
                    if(img!=null)
                    {
                        filename = DateTime.Now.ToString("dd-MM-yy-hh-mm-ss-") + img.FileName;
                        path = Path.Combine(Server.MapPath("~/Content/images/product"), filename);
                        img.SaveAs(path);
                        product.Images = filename; //Lưu ý
                    }
                    product.DateBegin = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                   /* product.Meta = Functions.ConvertToUnSign(product.Meta);*/ //convert Tiếng Việt không dấu
                    product.Order = getMaxOrder(product.CategoryId);
                    db.products.Add(product);
                    db.SaveChanges();
                    //return RedirectToAction("Index");
                    return RedirectToAction("Index", "product", new { Id = product.CategoryId });
                }
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(product);
        }

        // GET: admin2/product/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            getCategory(product.CategoryId);
            return View(product);
        }

        // POST: admin2/product/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,Name,Subname,Price,Images,Detail,Description,Meta,Hide,Order,CategoryId")] product product, HttpPostedFileBase img)
        {
            try
            {
                var path = "";
                var filename = "";
                product temp = db.products.Find(product.Id);
                if(ModelState.IsValid)
                {
                    if (img != null)
                    {
                        //filename = Guid.NewGuid().ToString() + img.FileName;
                        filename = DateTime.Now.ToString("dd-MM-yy-hh-mm-ss-") + img.FileName;
                        path = Path.Combine(Server.MapPath("~/Content/images/product"), filename);
                        img.SaveAs(path);
                        temp.Images = filename; //Lưu ý
                    }
                    else
                    {
                        temp.Images = temp.Images;
                    }
                    temp.Name = product.Name;
                    temp.Subname = product.Subname;
                    temp.Price = product.Price;
                    temp.Detail = product.Detail;
                    temp.Description = product.Description;
                    temp.Meta = product.Meta;
                    temp.Hide = product.Hide;
                    temp.Order = product.Order;
                    temp.CategoryId = product.CategoryId;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index", "product", new { Id = product.CategoryId });
                }

            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(product);
        }

        // GET: admin2/product/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: admin2/product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            product product = db.products.Find(id);
            db.products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public int getMaxOrder(long? CategoryId)
        {
            if (CategoryId == null)
                return 1;
            return db.products.Where(x => x.CategoryId == CategoryId).Count();
        }
    }
}
