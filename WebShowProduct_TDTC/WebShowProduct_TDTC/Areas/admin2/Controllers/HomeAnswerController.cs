﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Models;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class HomeAnswerController : Controller
    {
        private ShowProduct_TDTCEntities db = new ShowProduct_TDTCEntities();

        // GET: admin2/HomeAnswer
        public ActionResult Index(int? id = null)
        {
            getQuestion(id);
            return View(/*db.HomeAnswers.ToList()*/);
        }

        public void getQuestion(int? selectedId = null)
        {
            ViewBag.Question = new SelectList(db.HomeQuestions
                , "Id", "Question", selectedId);
        }

        public ActionResult getAnswer(int? id)
        {
            if (id == null)
            {
                var v = db.HomeAnswers.ToList();
                return PartialView(v);
            }
            var m = db.HomeAnswers.Where(x => x.QuestionID == id).ToList();
            return PartialView(m);
        }
        // GET: admin2/HomeAnswer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeAnswer homeAnswer = db.HomeAnswers.Find(id);
            if (homeAnswer == null)
            {
                return HttpNotFound();
            }
            return View(homeAnswer);
        }

        // GET: admin2/HomeAnswer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin2/HomeAnswer/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Answer,QuestionID")] HomeAnswer homeAnswer)
        {
            if (ModelState.IsValid)
            {
                db.HomeAnswers.Add(homeAnswer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(homeAnswer);
        }

        // GET: admin2/HomeAnswer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeAnswer homeAnswer = db.HomeAnswers.Find(id);
            if (homeAnswer == null)
            {
                return HttpNotFound();
            }
            getQuestion(homeAnswer.QuestionID);
            return View(homeAnswer);
        }

        // POST: admin2/HomeAnswer/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,Title,Answer,QuestionID")] HomeAnswer homeAnswer)
        {
           
         
            try
            {
                HomeAnswer temp = db.HomeAnswers.Find(homeAnswer.Id);
                if (ModelState.IsValid)
                {
                    temp.Title = homeAnswer.Title;
                    temp.Answer = homeAnswer.Answer;
                    temp.QuestionID = homeAnswer.QuestionID;
                    db.Entry(temp).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(homeAnswer);
        }

        // GET: admin2/HomeAnswer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeAnswer homeAnswer = db.HomeAnswers.Find(id);
            if (homeAnswer == null)
            {
                return HttpNotFound();
            }
            return View(homeAnswer);
        }

        // POST: admin2/HomeAnswer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HomeAnswer homeAnswer = db.HomeAnswers.Find(id);
            db.HomeAnswers.Remove(homeAnswer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
