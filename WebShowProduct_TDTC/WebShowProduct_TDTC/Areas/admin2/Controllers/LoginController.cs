﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShowProduct_TDTC.Areas.admin2.Models;
using WebShowProduct_TDTC.Areas.admin2.code;

using System.Web.Security;

namespace WebShowProduct_TDTC.Areas.admin2.Controllers
{
    public class LoginController : Controller
    {
        // GET: admin2/Login
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel model)
        {
            //var result = new AccountModel().Login(model.userName, model.Password);
            if (Membership.ValidateUser(model.userName,model.Password) && ModelState.IsValid)
            {
                //SessionHelper.SetSession(new UserSession() { userName = model.userName });
                FormsAuthentication.SetAuthCookie(model.userName, model.RememberMe);
                return RedirectToAction("Index", "Default");
            }
            else
            {
                ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không đúng!");   
            }
            return View(model);
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}