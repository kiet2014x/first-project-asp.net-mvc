﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShowProduct_TDTC.Areas.admin2.code
{
    public class SessionHelper
    {
        public static void SetSession(UserSession session)
        {
            HttpContext.Current.Session["loginSesstion"] = session;
        }
        public static UserSession GetSession()
        {
            var session = HttpContext.Current.Session["loginSession"];
            if (session == null)
                return null;
            else
            {
                return session as UserSession;
            }
        }

 
    }
}